import fnmatch
import os

for fname in os.listdir('.'):
    if fnmatch.fnmatch(fname, '*.cnf'):
        if "conv" not in fname:
            print fname
            inst_name = fname.split('.')[0]
            out_name = inst_name + '.conv.cnf'
            with open(fname, 'r') as in_f, open(out_name, 'w') as out_f:
                for in_line in in_f:
                    in_line_strip = in_line.strip()
                    if in_line_strip != "" and in_line_strip != "0":
                        if  in_line_strip[0] != "c" and in_line_strip[0] != "p":
                            print >> out_f, in_line_strip + " 0"
                        else:
                            print >> out_f, in_line_strip
