# Python script that generates new inst lists
import os

list_dir = "/home/chenwx/workspace/inst/lists"

in_list = "2014.indu.sat.laptop"
out_list = "2014.indu.sat.pre.laptop"

in_path = "{}/{}".format(list_dir, in_list)
out_path = "{}/{}".format(list_dir, out_list)

pre_base_path = "/home/chenwx/workspace/inst/sat14/app-pre"

with open(in_path, 'r') as in_f, open(out_path, 'w') as out_f:
    for in_line in in_f:
        inst, _ = in_line.split()
        out_path = "{}/{}.pre.cnf".format(pre_base_path, inst)

        # verify the pre cnf file exists
        if not os.path.isfile(out_path):
            print out_path, "doesn't exist"
            exit()

        print >>out_f, inst, out_path