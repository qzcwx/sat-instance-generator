# Python script for generating SAT instances
# allows variable clause length within one instance
# An example
# c  simple_v3_c2.cnf
# c 
# p cnf 3 2
# 2 3 -1 0
# 1 -3 0
import random

prefix = 'chen/'

seed = 0
# instNum = 10
instRange = range(0, 30)

# cv = 4.3
# cvRange = [3, 4.3, 6, 8, 10]
cvRange = [6, 10]
n = 50
tp = 'uf'

print
for cv in cvRange:
    for inst in instRange:
        random.seed(inst)
        m = int(n*cv)
        k = [3]*m
    
        inst = '{0}-n{1}-m{2}-i{3}.cnf'.format(tp, n, m, inst)
        print inst
        
        # fname = '{0}{1}'.format(prefix, inst)
        # with open(fname, 'w') as f:
        #     # print header
        #     print >>f, 'c {0}'.format(inst)
        #     print >>f, 'p cnf {0} {1}'.format(n,m)
        #     for i in xrange(m):
        #         # print k[i]
        #         sub = random.sample(range(n), k[i])
        #         # print sub
        #         for j in sub:
        #             if random.random()<0.5:
        #                 print >>f, '{0}'.format(j+1),
        #             else:
        #                 print >>f, '-{0}'.format(j+1),
        #         print >>f, '0'
